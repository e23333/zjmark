/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.zjmark.modules.markitem.web;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import com.jeeplus.modules.sys.entity.Role;
import com.jeeplus.modules.sys.utils.UserUtils;
import com.zjmark.modules.markitem.entity.MarkItemFile;
import com.zjmark.modules.markitem.entity.ReturnValue;
import com.zjmark.modules.markitem.service.MarkItemFileService;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.Encodes;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.sys.utils.DictUtils;
import com.zjmark.modules.markitem.entity.MarkItem;
import com.zjmark.modules.markitem.service.MarkItemService;

/**
 * 罚没商品信息Controller
 * @author lrz
 * @version 2017-06-26
 */
@Controller
@RequestMapping(value = "${adminPath}/markitem/markItem")
public class MarkItemController extends BaseController {

	@Autowired
	private MarkItemService markItemService;


	@Autowired
	private MarkItemFileService markItemFileService;

	@ModelAttribute
	public MarkItem get(@RequestParam(required=false) String id) {
		MarkItem entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = markItemService.get(id);
		}
		if (entity == null){
			entity = new MarkItem();
		}
		return entity;
	}
	

	/**
	 * 奢侈品信息列表页面
	 * @param markItem
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("markitem:markItem:list")
	@RequestMapping(value = {"list", ""})
	public String list(MarkItem markItem, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MarkItem> page = markItemService.findPage(new Page<MarkItem>(request, response), markItem);
		model.addAttribute("page", page);
		return "modules/luxury/luxuryInputList";
	}


	/**
	 * 查看，增加，编辑奢侈品信息
	 * @param markItem
	 * @param model
	 * @return
	 */
	@RequiresPermissions(value={"markitem:markItem:view","markitem:markItem:add","markitem:markItem:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MarkItem markItem, Model model) {
		model.addAttribute("markItem", markItem);
		MarkItemFile markItemFile = new MarkItemFile();
		markItemFile.setMarkItem(markItem);
		List<MarkItemFile> markItemFileList = markItemFileService.findList(markItemFile);
        model.addAttribute("markItemFileList", markItemFileList);
		return "modules/luxury/luxuryInputForm";
	}


	/**
	 * 保存奢侈品信息
	 * @param markItem
	 * @param model
	 * @param redirectAttributes
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequiresPermissions(value={"markitem:markItem:add","markitem:markItem:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MarkItem markItem, Model model, RedirectAttributes redirectAttributes,
					   HttpServletRequest request) throws Exception{
		if (!beanValidator(model, markItem)){
			return form(markItem, model);
		}

		if(!markItem.getIsNewRecord()){//编辑表单保存
			MarkItem t = markItemService.get(markItem.getId());   //从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(markItem, t);        //将编辑表单中的非NULL值覆盖数据库记录中的值
			markItemService.save(t);//保存
		}else{//新增表单保存
			markItemService.save(markItem);//保存
		}


        String[] fileidArr = request.getParameterValues("fileid");
        String[] shownameArr = request.getParameterValues("showname");
        String[] seqArr = request.getParameterValues("seq");

        if(fileidArr != null && fileidArr.length >0){
            int arrIndex = 0;
            for (String s : fileidArr) {
                MarkItemFile markItemFile = markItemFileService.get(s);
                markItemFile.setMarkItem(markItem);
                markItemFile.setShowname(shownameArr[arrIndex]);
                markItemFile.setSeq(Short.parseShort(seqArr[arrIndex]));
                markItemFileService.save(markItemFile);
                arrIndex++;
            }

            MarkItemFile markItemFile = new MarkItemFile();
            markItemFile.setMarkItem(markItem);
            List<MarkItemFile> markItemFiles = markItemFileService.findList(markItemFile);

            for (MarkItemFile itemFile : markItemFiles) {

                List<String> formFileIds = Arrays.asList(fileidArr);
                if(!formFileIds.contains(itemFile.getId())){
                    markItemFileService.delete(itemFile);
                }

            }
        }


        addMessage(redirectAttributes, "保存奢侈品信息成功");
		return "redirect:"+Global.getAdminPath()+"/markitem/markItem/?repage";
	}


    /**
     * 批量上报
     */
    @RequestMapping(value = "subMulti")
    public String subMulti(String ids, RedirectAttributes redirectAttributes) {
        String idArray[] =ids.split(",");
        StringBuffer msg = new StringBuffer();
        for(String id : idArray){
            MarkItem markItem = markItemService.get(id);
            markItem.setAuditstatus("1");
            markItemService.save(markItem);
        }
        addMessage(redirectAttributes, "操作成功");
        return "redirect:"+Global.getAdminPath()+"/markitem/markItem/?repage";
    }

	@RequestMapping("uploadone")
	@ResponseBody
	public ReturnValue uploadone(@RequestParam("file")MultipartFile file, HttpServletRequest request) throws IOException {
		// 用于存储返回值
		String fileName = saveImg(file,request);

		ReturnValue returnValue = new ReturnValue();
		returnValue.setCode(0);
		returnValue.setMsg("上传成功");
		returnValue.setData("upload/"+fileName);

		return returnValue;
	}

	@RequestMapping("uploadall")
	@ResponseBody
	public ReturnValue uploadMulti(@RequestParam("file")MultipartFile file, HttpServletRequest request) throws IOException {
		// 用于存储返回值
        // 上传附件
        String fileName = saveImg(file,request);

        // 记录信息
        MarkItemFile markItemFile = new MarkItemFile();
        markItemFile.setUrl("upload/"+fileName);
        markItemFile.setFilename(fileName);
        markItemFileService.save(markItemFile);

		ReturnValue returnValue = new ReturnValue();
        returnValue.setCode(0);
        returnValue.setMsg("上传成功");
        returnValue.setData(markItemFile);
		return returnValue;
	}

    /**
     * 删除操作
     *
     * @return ReturnValue
     */
    @RequestMapping("delfile")
    @ResponseBody
    public ReturnValue delfile(String id) {
        // 只进行逻辑删除，且不会删除硬盘中附件

        markItemFileService.delete(markItemFileService.get(id));
        ReturnValue returnValue = new ReturnValue();
        returnValue.setCode(0);
        returnValue.setMsg("成功");
        return returnValue;
    }


	/**
	 * 
	 * 导出
	 */
	@RequestMapping(value = "export")
    public void export( HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
		String fileName ="罚没商品信息.xlsx";
		// 工作薄
		XSSFWorkbook wb = new XSSFWorkbook();
		//工作表
		XSSFSheet sheet = wb.createSheet("罚没商品信息");
		
		sheet.setDefaultColumnWidth(20);
		sheet.setColumnWidth(0, 7 * 256);
		//表头样式
		XSSFCellStyle titleStyle = wb.createCellStyle();
		titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);//居中 
		titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);// 垂直
		/** 字体begin */
		// 背景颜色
		titleStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		// 生成一个字体
					XSSFFont TitleFont = wb.createFont();
					TitleFont.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index
															// //字体颜色
					TitleFont.setFontHeightInPoints((short) 10);
					TitleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 字体增粗
					// 把字体应用到当前的样式
					titleStyle.setFont(TitleFont);
					/** 字体end */
					
					//样式
					XSSFCellStyle cellStyle = wb.createCellStyle();
					cellStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);//居中 
					cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);// 垂直
					XSSFFont cellFont = wb.createFont();
					cellFont.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index
					cellFont.setFontHeightInPoints((short) 10);
					cellStyle.setFont(cellFont);
					
					
					// 第一行数据
					XSSFRow rowTitle = sheet.createRow(0);
					String[] title = new String[]{"序号","箱子号","英文品牌","中文品牌","种类","原产地","材质名","百分比","使用位置","二维码","需补录标志","状态","最初绑定时间","操作人"};
					for(int i=0;i<title.length;i++){
						Cell cell = rowTitle.createCell(i);
						cell.setCellStyle(titleStyle);
						cell.setCellValue(title[i]);
					}
					//填写数据
					MarkItem markItem=new MarkItem();
			        markItem.setOrigin(request.getParameter("origin"));
			        markItem.setBrand(request.getParameter("brand"));
			        markItem.setTypes(request.getParameter("types"));
			        markItem.setQrcode(request.getParameter("qrcode"));
					List<MarkItem> list=markItemService.findList(markItem);


					int i=1;
					int j=1;
			        List<XSSFRow> rows = new ArrayList<XSSFRow>();
					for(MarkItem m:list){


						//合并单元格
						//如果没有材质，则不合并单元格,并且要创建一个空的行




						j=j+1;
					}
					// 写文件
					response.reset();
					response.setContentType("application/octet-stream; charset=utf-8");
					response.setHeader("Content-Disposition", "attachment; filename="+ Encodes.urlEncode(fileName));
					wb.write(response.getOutputStream());
		   } catch (Exception e) {
			e.printStackTrace();
		}
		
    }
	
	



	/**
	 * 箱子中注销罚没商品信息
	 */
	@RequiresPermissions("markitem:markItem:edit")
	@RequestMapping(value = "zx")
	public String zx(MarkItem markItem, RedirectAttributes re) {
		markItemService.save(markItem);
		addMessage(re, "注销罚没商品信息成功");
		return "redirect:"+Global.getAdminPath()+"/markitem/markItem/xzsp?repage";
	}
	

	

	/**
	 * 删除罚没商品信息
	 */
	@RequiresPermissions("markitem:markItem:del")
	@RequestMapping(value = "delete")
	public String delete(MarkItem markItem, RedirectAttributes redirectAttributes) {
		markItemService.delete(markItem);
		addMessage(redirectAttributes, "删除奢侈品信息成功");
		return "redirect:"+Global.getAdminPath()+"/markitem/markItem/?repage";
	}

	/**
	 * 批量删除罚没商品信息
	 */
	@RequiresPermissions("markitem:markItem:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			markItemService.delete(markItemService.get(id));
		}
		addMessage(redirectAttributes, "删除罚没商品信息成功");
		return "redirect:"+Global.getAdminPath()+"/markitem/markItem/?repage";
	}
	

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("markitem:markItem:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MarkItem> list = ei.getDataList(MarkItem.class);
			for (MarkItem markItem : list){
				try{
					markItem.setAuditstatus("0");
					markItemService.save(markItem);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条奢侈品信息。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条奢侈品信息"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入奢侈品信息失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/markitem/markItem/?repage";
    }
	
	/**
	 * 下载导入罚没商品信息数据模板
	 */
	@RequiresPermissions("markitem:markItem:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			response.setCharacterEncoding("utf-8");
            String fileName = "奢侈品信息导入模板.xlsx";
    		List<MarkItem> list = Lists.newArrayList(); 
    		new ExportExcel("奢侈品信息", MarkItem.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/markitem/markItem/?repage";
    }




	private String saveImg(MultipartFile t ,HttpServletRequest request){
		String realPath = request.getSession().getServletContext().getRealPath("/upload");
		String fileName = UUID.randomUUID().toString().replaceAll("-", "") + t.getOriginalFilename().substring(t.getOriginalFilename().lastIndexOf("."));
		try {
			FileUtils.copyInputStreamToFile(t.getInputStream(), new File(realPath, fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileName;
	}

}