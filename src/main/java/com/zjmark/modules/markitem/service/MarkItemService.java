/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.zjmark.modules.markitem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import com.jeeplus.common.utils.StringUtils;
import com.zjmark.modules.markitem.entity.MarkItem;
import com.zjmark.modules.markitem.dao.MarkItemDao;

/**
 * 罚没商品信息Service
 * @author lrz
 * @version 2017-06-26
 */
@Service
@Transactional(readOnly = true)
public class MarkItemService extends CrudService<MarkItemDao, MarkItem> {
	public MarkItem get(String id) {
		MarkItem markItem=super.get(id);
		//markItem.setChildList(markMaterialDao.findList(new MarkMaterial(markItem)));
		return markItem;
	}
	
	public List<MarkItem> findList(MarkItem markItem) {
		return super.findList(markItem);
	}
	
	public Page<MarkItem> findPage(Page<MarkItem> page, MarkItem markItem) {
		return super.findPage(page, markItem);
	}
	
	@Transactional(readOnly = false)
	public void save(MarkItem markItem) {
		super.save(markItem);
	}
	
	
	@Transactional(readOnly = false)
	public void delete(MarkItem markItem) {
		super.delete(markItem);
	}

	public void cz(List<MarkItem> page) {
	    for(MarkItem mk :page){
	    	String kk="";

//	    	mk.setCz(kk);
	    }	
	}

}