/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.zjmark.modules.markitem.service;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import com.zjmark.modules.markitem.dao.MarkItemFileDao;
import com.zjmark.modules.markitem.entity.MarkItem;
import com.zjmark.modules.markitem.entity.MarkItemFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 罚没商品信息Service
 * @author lrz
 * @version 2017-06-26
 */
@Service
@Transactional(readOnly = true)
public class MarkItemFileService extends CrudService<MarkItemFileDao, MarkItemFile> {
    public MarkItemFile get(String id) {
        MarkItemFile markItemFile = super.get(id);
        return markItemFile;
    }

    public List<MarkItemFile> findList(MarkItemFile markItemFile) {
        return super.findList(markItemFile);
    }

    public Page<MarkItemFile> findPage(Page<MarkItemFile> page, MarkItemFile markItemFile) {
        return super.findPage(page, markItemFile);
    }

    @Transactional(readOnly = false)
    public void save(MarkItemFile markItemFile) {
        super.save(markItemFile);
    }


    @Transactional(readOnly = false)
    public void delete(MarkItemFile markItemFile) {
        super.delete(markItemFile);
    }

}