package com.zjmark.modules.markitem.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Package com.zjmark.modules.markitem.entity
 * @Description: Json格式的返回对象
 * @User: mather
 * @Date: 2018-11-05
 * @Time: 上午9:59
 * @Since: 2018-11-05-上午9:59
 * @Version V1.0
 */

@Data
public class ReturnValue {
    private Integer code;
    private String msg;
    private Object data;
    private List<Object> list;
}
