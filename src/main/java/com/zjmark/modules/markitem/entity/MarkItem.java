/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.zjmark.modules.markitem.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;
import lombok.Data;

/**
 * 罚没商品信息Entity
 * @author lrz
 * @version 2017-06-26
 */
@Data
public class MarkItem extends DataEntity<MarkItem> {
	
	private static final long serialVersionUID = 1L;

	private String itemname;		   // 产品名称
	private String brand;		       // 品牌
	private String zhongbrand;		   // 品牌
	private String requestcode;        // 申请号
	private String types;		       // 种类
	private String securitycode;       // 品牌防伪标
	private String size;		       // 尺寸
	private String modelinfo;          // 型号
	private String material;           // 材质
	private String diameter;           // 表径
	private String maintenanceflag;    // 是否保养：0否1是
	private String fineness;  		   // 成色
	private String price; 		       // 价格
	private String finalprice;		   // 公价
	private String styleinfo; 		   // 款式
	private String otherinfo; 		   // 其它
	private String zhengimg;		   // 主照片
	private String fanimg;		       // 带标签附照片
	private String mcode;		       // 标签值
	private String qrcode;		       // 二维码内容
	private String auditstatus;  	   // 审核状态：0未提交1待审核2审核通过3审核驳回

	private String origin;		       // 原产地
//	private String caizhiimg;		   // 材质照片url
//	private String biaoqianimg;		   // 贴签后照片url
//	private String otherimg;		   // 其他照片url
//	private String sub;		           // 副标
//	private String needsupply;		   // 需补录标志 0否1是
//	private Date firstinputtime;       // 最初绑定时间
//	private String cz;                 // 为了前台好显示，拼接
//	private String cancelmark;         // 注销标志
//	private String typesshow;		   // 种类是否显示
//	private String zhongbrandshow;     // 品牌是否显示
//	private String brandshow;		   // 品牌是否显示
//	private String originshow;		   // 原产地是否显示
//	private String itemnameshow;       // 产品名称是否显示
//	private String subshow;		       // 副标是否显示
//	private String otherimgshow;	   // 其他照片是否显示
//	private String biaoqianimgshow;	   // 贴签后照片是否显示
//	private String caizhiimgshow;	   // 材质照片是否显示
//	private String fanimgshow;		   // 反面照片是否显示
//	private String zhengimgshow;	   // 正面照片是否显示
//	private String sizeshow;		   // 尺寸是否显示
//	private String materialshow;       // 材质是否显示

	private List<MarkItemFile> markItemFileList = Lists.newArrayList();


	@ExcelField(title="申请号",  align=2, sort=1)
	public String getRequestcode() {
		return requestcode;
	}

	@ExcelField(title="英文品牌", dictType="item_brand", align=2, sort=3)
	public String getBrand() {
		return brand;
	}

	@ExcelField(title="中文品牌", align=2, sort=5)
	public String getZhongbrand() {
		return zhongbrand;
	}

	@ExcelField(title="类别", dictType="item_kind", align=2, sort=7)
	public String getTypes() {
		return types;
	}

	@ExcelField(title="品牌防伪标", align=2, sort=9)
	public String getSecuritycode() {
		return securitycode;
	}

	@ExcelField(title="尺寸", align=2, sort=11)
	public String getSize() {
		return size;
	}

	@ExcelField(title="型号（腕表填写）", align=2, sort=13)
	public String getModelinfo() {
		return modelinfo;
	}

	@ExcelField(title="材质（腕表填写）", align=2, sort=15)
	public String getMaterial() {
		return material;
	}

	@ExcelField(title="表径（腕表填写）", align=2, sort=17)
	public String getDiameter() {
		return diameter;
	}

	@ExcelField(title="是否保养", dictType="yes_no", align=2, sort=19)
	public String getMaintenanceflag() {
		return maintenanceflag;
	}

	@ExcelField(title="成色", align=2, sort=21)
	public String getFineness() {
		return fineness;
	}

	@ExcelField(title="价格", align=2, sort=23)
	public String getPrice() {
		return price;
	}

	@ExcelField(title="公价", align=2, sort=25)
	public String getFinalprice() {
		return finalprice;
	}

}