package com.zjmark.modules.markitem.entity;

import com.jeeplus.common.persistence.DataEntity;
import lombok.Data;

/**
 * @Package com.zjmark.modules.markitem.entity
 * @Description: 奢饰品附加文件表
 * @User: mather
 * @Date: 2018-11-01
 * @Time: 下午3:25
 * @Since: 2018-11-01-下午3:25
 * @Version V1.0
 */

@Data
public class MarkItemFile extends DataEntity<MarkItemFile> {

    private static final long serialVersionUID = 1L;

    private MarkItem markItem;    //奢侈品信息表
    private String url;           //url
    private String showname;      //图片描述
    private String filename;      //文件名
    private short seq;            //序号

    public MarkItem getMarkItem() {
        return markItem;
    }

    public void setMarkItem(MarkItem markItem) {
        this.markItem = markItem;
    }
}
