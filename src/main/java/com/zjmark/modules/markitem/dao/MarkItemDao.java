/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.zjmark.modules.markitem.dao;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import com.zjmark.modules.markitem.entity.MarkItem;

/**
 * 罚没商品信息DAO接口
 * @author lrz
 * @version 2017-06-26
 */
@MyBatisDao
public interface MarkItemDao extends CrudDao<MarkItem> {
}