package com.zjmark.modules.markitem.dao;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import com.zjmark.modules.markitem.entity.MarkItemFile;

/**
 * @Package com.zjmark.modules.markitem.dao
 * @Description: 奢侈品附件表DAO
 * @User: mather
 * @Date: 2018-11-01
 * @Time: 下午4:21
 * @Since: 2018-11-01-下午4:21
 * @Version V1.0
 */
@MyBatisDao
public interface MarkItemFileDao extends CrudDao<MarkItemFile> {

}
