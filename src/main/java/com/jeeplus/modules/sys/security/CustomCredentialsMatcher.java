package com.jeeplus.modules.sys.security;

import com.jeeplus.common.security.Digests;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;


/**
 * Created by s3 on 2017-5-2.
 */
public class CustomCredentialsMatcher extends SimpleCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken authcToken,
                                      AuthenticationInfo info) {
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;

        Object tokenCredentials = encrypt(String.valueOf(token.getPassword()));
        Object accountCredentials = getCredentials(info);
        return equals(tokenCredentials, accountCredentials);
    }

    // 将传进来密码加密方法
    private String encrypt(String data) {

        String sha384Hex = Digests.string2MD5(data);
        ;// 这里可以选择自己的密码验证方式 比如 md5或者sha256等
        return sha384Hex;
    }
}