<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
  <title>修改密码</title>
  <meta name="decorator" content="default"/>
  <script type="text/javascript">
    var validateForm;
    function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
      if(validateForm.form()){
        $("#inputForm").submit();
        return true;
      }

      return false;
    }
    $(document).ready(function() {
      $("#oldPassword").focus();
      validateForm = $("#inputForm").validate({
        submitHandler: function(form){
          loading('正在提交，请稍等...');
          form.submit();
        },
        errorContainer: "#messageBox",
        errorPlacement: function(error, element) {
          $("#messageBox").text("输入有误，请先更正。");
          if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
            error.appendTo(element.parent().parent());
          } else {
            error.insertAfter(element);
          }
        }
      });

    });
  </script>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content">
  <div class="ibox">
    <div class="ibox-title">
      <h5>登录密码修改 </h5>
      <div class="ibox-tools">
        <a class="collapse-link">
          <i class="fa fa-chevron-up"></i>
        </a>
        <a class="close-link">
          <i class="fa fa-times"></i>
        </a>
      </div>
    </div>
    <div class="ibox-content">
      <sys:message content="${message}"/>
      <div class="row">
        <div class="col-sm-12">
          <form:form id="inputForm" modelAttribute="user" action="${ctx}/password/password/save"  method="post" class="form-horizontal form-group">
            <div class="control-group">
              <label class="col-sm-3 control-label"><font color="red">*</font>旧密码:</label>
              <div class="controls">
                <input id="oldPassword" name="oldPassword" type="password" value="" maxlength="50" minlength="3"  class="form-control  max-width-250 required"/>
              </div>
            </div>
            <div class="control-group">
              <label class="col-sm-3 control-label"><font color="red">*</font>新密码:</label>
              <div class="controls">
                <input id="newPassword" name="newPassword" type="password" value="" maxlength="50" minlength="3" class="form-control  max-width-250 required"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><font color="red">*</font>确认新密码:</label>
              <div class="controls">
                <input id="validatePassword" name="validatePassword" type="password" value="" maxlength="50" minlength="3" class="form-control  max-width-250 required" equalTo="#newPassword"></input>
              </div>
            </div>
            <div class="form-actions">
              <%--<input id="btnSubmit" class="btn btn-primary" type="submit"  value="保 存"/>--%>
            </div>
        </div>
      </div>
      </form:form>

      <div class="row">
        <div class="col-sm-12">

          <div class="pull-right">
            <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="doSubmit()" ><i class="fa fa-search"></i> 确定</button>
            <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</body>
</html>