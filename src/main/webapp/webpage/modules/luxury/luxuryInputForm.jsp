<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
    <title>罚没商品信息管理</title>
    <meta name="decorator" content="default"/>
    <style>
        .layui-upload-img{width: 92px; height: 92px; margin: 0 10px 10px 0;}
    </style>

    <script type="text/javascript">
        var validateForm;
        function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
            if(validateForm.form()){
                $("#inputForm").submit();
                return true;
            }

            return false;
        }

        $(document).ready(function() {
            validateForm = $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });


        });
    </script>
</head>
<body class="hideScroll">
<form:form id="inputForm" modelAttribute="markItem" action="${ctx}/markitem/markItem/save" method="post" class="form-horizontal">
<form:hidden path="id"/>
<form:hidden path="auditstatus" value="0"/>
<sys:message content="${message}"/>
<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
    <tbody>
    <tr>
        <td class="width-15 active"><label class="pull-right">申请号：</label></td>
        <td class="width-35" colspan="3">
            <form:input path="requestcode" htmlEscape="false" maxlength="32" class="form-control required"/>
        </td>
    </tr>
    <tr>
        <td class="width-15 active"><label class="pull-right">品牌：</label></td>
        <td class="width-35" colspan="3">
            <form:select path="brand" class="form-control required">
                <form:option value="" label=""/>
                <form:options items="${fns:getDictList('item_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
            </form:select>
        </td>
    </tr>
    <tr>
        <td class="width-15 active"><label class="pull-right">种类：</label></td>
        <td class="width-35">
            <form:select path="types" class="form-control ">
                <form:option value="" label=""/>
                <form:options items="${fns:getDictList('item_kind')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
            </form:select>
        </td>
        <td class="width-15 active"><label class="pull-right">品牌防伪标：</label></td>
        <td class="width-35">
            <form:input path="securitycode" htmlEscape="false"  maxlength="64"  class="form-control "/>
        </td>
    </tr>
    <tr>
        <td class="width-15 active"><label class="pull-right">尺寸：</label></td>
        <td class="width-35">
            <form:input path="size" htmlEscape="false"  maxlength="32"  class="form-control "/>
        </td>
        <td class="width-15 active"><label class="pull-right">型号(腕表)：</label></td>
        <td class="width-35">
            <form:input path="modelinfo" htmlEscape="false" maxlength="64" class="form-control "/>
        </td>
    </tr>
    <tr>
        <td class="width-15 active"><label class="pull-right">材质(腕表)：</label></td>
        <td class="width-35">
            <form:input path="material" htmlEscape="false"  maxlength="64"  class="form-control "/>
        </td>
        <td class="width-15 active"><label class="pull-right">表径(腕表)：</label></td>
        <td class="width-35">
            <form:input path="diameter" htmlEscape="false" maxlength="32" class="form-control "/>
        </td>
    </tr>
    <tr>
        <td class="width-15 active"><label class="pull-right">是否保养：</label></td>
        <td class="width-35">
            <form:select path="maintenanceflag" class="form-control ">
                <form:option value="" label=""/>
                <form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
            </form:select>
        </td>
        <td class="width-15 active"><label class="pull-right">成色：</label></td>
        <td class="width-35">
            <form:input path="fineness" htmlEscape="false"  maxlength="64"  class="form-control "/>
        </td>
    </tr>
    <tr>
        <td class="width-15 active"><label class="pull-right">价格：</label></td>
        <td class="width-35">
            <form:input path="price" htmlEscape="false" maxlength="10" class="form-control number required"/>
        </td>
        <td class="width-15 active"><label class="pull-right">公价：</label></td>
        <td class="width-35">
            <form:input path="finalprice" htmlEscape="false" maxlength="10" class="form-control number "/>
        </td>
    </tr>
    <tr>
        <td class="width-15 active"><label class="pull-right">标签值：</label></td>
        <td class="width-35">
            <form:input path="mcode" htmlEscape="false" class="form-control "/>
        </td>
        <td class="width-15 active"><label class="pull-right">二维码内容：</label></td>
        <td class="width-35">
            <form:input path="qrcode" htmlEscape="false" class="form-control "/>
        </td>
    </tr>

    <tr>
        <td class="width-15 active"><label class="pull-right">主照片：</label></td>
        <td class="width-35">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="zhengbtn"><i class="layui-icon">&#xe67c;</i>
                    <c:if test="${markItem.zhengimg != null && markItem.zhengimg != ''}">
                        重新选择
                    </c:if>
                    <c:if test="${markItem.zhengimg == null || markItem.zhengimg == ''}">
                        上传图片
                    </c:if>
                </button>
                <div class="layui-upload-list">
                    <form:hidden path="zhengimg" htmlEscape="false"/>
                    <img class="layui-upload-img" id="zhengshow"
                        <c:if test="${markItem.zhengimg != null && markItem.zhengimg != ''}">
                           <img src="${pageContext.request.contextPath}/${markItem.zhengimg}"
                        </c:if>
                    >
                    <p id="zhengText"></p>
                </div>
            </div>
        </td>
        <td class="width-15 active"><label class="pull-right">副照片：</label></td>
        <td class="width-35">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="fanbtn"><i class="layui-icon">&#xe67c;</i>
                    <c:if test="${markItem.fanimg != null && markItem.fanimg != ''}">
                        重新选择
                    </c:if>
                    <c:if test="${markItem.fanimg == null || markItem.fanimg == ''}">
                        上传图片
                    </c:if>
                </button>
                <div class="layui-upload-list">
                    <form:hidden path="fanimg" htmlEscape="false"/>
                    <img class="layui-upload-img" id="fanshow"
                        <c:if test="${markItem.fanimg != null && markItem.fanimg != ''}">
                            <img src="${pageContext.request.contextPath}/${markItem.fanimg}"
                        </c:if>
                    >
                    <p id="fanText"></p>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>
<div class="layui-upload">
    <button type="button" class="layui-btn layui-btn-normal" id="testList">更多照片</button>
    <div class="layui-upload-list">
        <table class="layui-table">
            <thead>
            <tr><th>图片</th>
                <th>描述</th>
                <th>排序</th>
                <th>状态</th>
                <th>操作</th>
            </tr></thead>
            <tbody id="demoList">
            <c:forEach items="${markItemFileList}" var="markItemFile">
                <tr id="${markItemFile.id}">
                    <td><img src="${pageContext.request.contextPath}/${markItemFile.url}" alt="${markItemFile.filename}" class="layui-upload-img"><input id="fileid-" name="fileid" type="hidden" value="${markItemFile.id}"></td>
                    <td><input id="showname" name="showname" type="text" value="${markItemFile.showname}"></td>
                    <td><input id="seq" name="seq" type="text" value="${markItemFile.seq}"></td>
                    <td>已上传</td>
                    <td><div class="layui-btn layui-btn-xs layui-btn-danger demo-delete" onclick="delThis('${markItemFile.id}')">删除</div></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <button type="button" class="layui-btn" id="testListAction">开始上传</button>
</div>


</form:form>
<script>
    layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;

        //正图1
        var uploadInst = upload.render({
            elem: '#zhengbtn'
            ,url: '${ctx}/markitem/markItem/uploadone'
            ,accept: 'images' //图片
            ,acceptMime: 'image/*'
            ,size: 4 * 1024
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#zhengshow').attr('src', result); //图片链接（base64）
                });
            }
            ,done: function(res){
                //如果上传失败

                if(res.code > 0){
                    return layer.msg('上传失败');
                }
                //上传成功，文件位置回传
                $('#zhengimg').attr('value', res.data);
            }
            ,error: function(){
                //演示失败状态，并实现重传
                var zhengText = $('#zhengText');
                zhengText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                zhengText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });

        var uploadInst2 = upload.render({
            elem: '#fanbtn'
            ,url: '${ctx}/markitem/markItem/uploadone'
            ,accept: 'images' //图片
            ,acceptMime: 'image/*'
            ,size: 4 * 1024
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#fanshow').attr('src', result); //图片链接（base64）
                });
            }
            ,done: function(res){
                //如果上传失败
                if(res.code > 0){
                    return layer.msg('上传失败');
                }
                //上传成功, 文件位置回传
                $('#fanimg').attr('value', res.data);
            }
            ,error: function(){
                //演示失败状态，并实现重传
                var fanText = $('#fanText');
                fanText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                fanText.find('.demo-reload').on('click', function(){
                    uploadInst2.upload();
                });
            }
        });




        //多文件列表示例
        var demoListView = $('#demoList')
            ,uploadListIns = upload.render({
            elem: '#testList'
            ,url: '${ctx}/markitem/markItem/uploadall'
            ,data: {
                showname: "", seq: 0
            }
            ,accept: 'images' //图片
            ,acceptMime: 'image/*'
            ,size: 4 * 1024
            ,multiple: true
            ,auto: false
            ,bindAction: '#testListAction'
            ,choose: function(obj){
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    var tr = $(['<tr id="upload-'+ index +'">'
                        ,'<td>'+ '<img src="'+ result +'" alt="'+ file.name +'" class="layui-upload-img"><input id="fileid-' + index + '" name="fileid" type="hidden">' +'</td>'
                        ,'<td><input id="showname-' + index + '" name="showname" type="text"></td>'
                        ,'<td><input id="seq-' + index + '" name="seq" type="text" value="' + index.substring(index.indexOf("-")+1,index.length) + '"></td>'
                        ,'<td>等待上传</td>'
                        ,'<td>'
                        ,'<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                        ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

//                    tr.find('#showname-' + index).on('change', function(){
//                        uploadListIns.config.data.showname = $("#showname-" + index).val();
//                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    demoListView.append(tr);
                });
            }
            ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
//                obj.preview(function(index, file, result){
//                    uploadListIns.config.data.showname = $("#showname-" + index).val();
//                    uploadListIns.config.data.seq  = $("#seq-" + index).val();
//                    console.log(uploadListIns.config.data.showname); //得到文件索引
//                    console.log(uploadListIns.config.data.seq); //得到文件索引
//                    obj.upload(index, file);
//                });
                layer.load(2,{time: 1000}); //上传loading
            }
            ,done: function(res, index, upload){
                if(res.code == 0){ //上传成功
                    var tr = demoListView.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tr.find('#fileid-'+ index).val(res.data.id);
                    tds.eq(3).html('<span style="color: #5FB878;">上传成功</span>');
                    //tds.eq(3).html(''); //清空操作
                    return delete this.files[index]; //删除文件队列已经上传成功的文件
                }
                this.error(index, upload);
            }
            ,error: function(index, upload){
                var tr = demoListView.find('tr#upload-'+ index)
                    ,tds = tr.children();
                tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
        });




    });

    /**
     * 删除附件
     */
    function delThis(id) {
        $("#" + id + "").remove();
    }
</script>
</body>
</html>