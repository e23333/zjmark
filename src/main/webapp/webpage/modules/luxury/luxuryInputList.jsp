<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
    <title>罚没商品信息管理</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
        });
    </script>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-title">
            <h5>奢侈品信息列表</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>

        <div class="ibox-content">
            <sys:message content="${message}"/>

            <!--查询条件-->
            <div class="row">
                <div class="col-sm-12">
                    <form:form id="searchForm" modelAttribute="markItem" action="${ctx}/markitem/markItem/" method="post" class="form-inline">
                        <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
                        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
                        <table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
                        <div class="form-group">
                            <span>审核状态：</span>
                            <form:select path="auditstatus"  class="form-control m-b">
                                <form:option value="" label=""/>
                                <form:options items="${fns:getDictList('item_auditstatus')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
                            </form:select>
                            <span>类型：</span>
                            <form:select path="types"  class="form-control m-b">
                                <form:option value="" label=""/>
                                <form:options items="${fns:getDictList('item_kind')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
                            </form:select>
                            <span>申请号：</span>
                            <form:input path="requestcode"/>
                            <span>标签值：</span>
                            <form:input path="mcode"/>
                            <span>品牌：</span>
                            <form:select path="brand"  class="form-control m-b">
                                <form:option value="" label=""/>
                                <form:options items="${fns:getDictList('item_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
                            </form:select>
                        </div>
                    </form:form>
                    <br/>
                </div>
            </div>

            <!-- 工具栏 -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-left">
                        <shiro:hasPermission name="markitem:markItem:add">
                            <table:addRow url="${ctx}/markitem/markItem/form" title=""></table:addRow><!-- 增加按钮 -->
                            <table:delRow url="${ctx}/markitem/markItem/subMulti" id="contentTable" label="批量上报"></table:delRow><!-- 删除按钮 -->
                        </shiro:hasPermission>
                        <table:importExcel url="${ctx}/markitem/markItem/import"></table:importExcel><!-- 导入按钮 -->
                        <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
                    </div>
                    <div class="pull-right">
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
                        <button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
                    </div>
                </div>
            </div>

            <!-- 表格 -->
            <table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
                <thead>
                <tr>
                    <th> <input type="checkbox" class="i-checks"></th>
                    <th  class="sort-column requestcode">申请号</th>
                    <th  class="sort-column brand">英文品牌</th>
                    <th  class="sort-column brand">中文品牌</th>
                    <th  class="sort-column types">类型</th>
                    <th  class="sort-column price">价格</th>
                    <th  class="sort-column mcode">标签值</th>
                    <th  class="sort-column createBy.id">操作人</th>
                    <th  class="sort-column auditstatus">状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${page.list}" var="markItem">
                    <tr>
                        <td> <input type="checkbox" id="${markItem.id}" class="i-checks"></td>
                        <td>
                                ${markItem.requestcode}
                        </td>
                        <td>
                                ${fns:getDictLabel(markItem.brand, 'item_brand', '')}
                        </td>
                        <td>
                                ${fns:getDictDescription(markItem.brand, 'item_brand', '')}
                        </td>
                        <td>
                                ${fns:getDictLabel(markItem.types, 'item_kind', '')}
                        </td>
                        <td>
                                ${markItem.price}
                        </td>
                        <td>
                                ${markItem.mcode}
                        </td>
                        <td>
                                ${markItem.createBy.name}
                        </td>
                        <td>
                                ${fns:getDictLabel(markItem.auditstatus, 'item_auditstatus', '')}
                        </td>
                        <td>
                            <c:if test="${markItem.auditstatus == '0' || markItem.auditstatus == '3'}">
                                <shiro:hasPermission name="markitem:markItem:edit">
                                    <a href="#" onclick="openDialog('修改', '${ctx}/markitem/markItem/form?id=${markItem.id}','900px', '600px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
                                </shiro:hasPermission>
                                <shiro:hasPermission name="markitem:markItem:del">
                                    <a href="${ctx}/markitem/markItem/delete?id=${markItem.id}" onclick="return confirmx('确认要删除吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
                                </shiro:hasPermission>
                            </c:if>
                            <shiro:hasPermission name="markitem:markItem:edit">
                                <a href="#" onclick="openDialog('查看', '${ctx}/markitem/markItem/form?id=${markItem.id}','900px', '600px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 查看</a>
                            </shiro:hasPermission>
                                <%--<shiro:hasPermission name="markitem:markItem:edit">--%>
                                    <%--<a href="${ctx}/markitem/markItem/zx2?id=${markItem.id}"   class="btn btn-warning btn-xs " ><i class="fa fa-edit"></i> 注销</a>--%>
                                <%--</shiro:hasPermission>--%>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <!-- 分页代码 -->
            <table:page page="${page}"></table:page>
            <br/>
            <br/>
        </div>
    </div>
</div>

<script>
    layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;
        //绑定原始文件域
        upload.render({
            elem: '#test20'
            ,url: '/upload/'
            ,done: function(res){
                console.log(res)
            }
        });

    });
</script>
</body>
</html>