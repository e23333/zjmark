<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>保存箱子管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="markBox" action="${ctx}/markbox/markBox/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-10 active"><label class="pull-left">二维码内容：</label></td>
					<td class="width-25">
						<form:input path="qrcode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-10 active"><label class="pull-left">箱子号：</label></td>
					<td class="width-20">
						<form:input path="mcode" htmlEscape="false"    class="form-control "/>
					</td>
					
				</tr>
				<tr>
				<td class="width-10 active"><label class="pull-left">品牌：</label></td>
					<td class="width-25">
                          <form:select path="brand" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('item_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						   </form:select>					
					</td>
					<td class="width-10 active"><label class="pull-left">种类：</label></td>
					<td class="width-25">
						<form:select path="types" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('item_kind')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>