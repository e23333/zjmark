<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>保存箱子管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>保存箱子列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="markBox" action="${ctx}/markbox/markBox/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>箱子号：</span>
			<form:input path="mcode"/>
			<span>品牌：</span>
                   <form:select path="brand"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('item_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				  </form:select>							  
		    <span>种类：</span>
				<form:select path="types"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('item_kind')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="markbox:markBox:add">
				<table:addRow url="${ctx}/markbox/markBox/form" title="保存箱子"></table:addRow><!-- 增加按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="markbox:markBox:edit">
			    <table:editRow url="${ctx}/markbox/markBox/form" title="保存箱子" id="contentTable"></table:editRow><!-- 编辑按钮 -->
			</shiro:hasPermission>
			   <table:exportExcel url="${ctx}/markbox/markBox/export"></table:exportExcel><!-- 导出按钮 -->
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th> <input type="checkbox" class="i-checks"></th>
				<th  class="sort-column mcode">箱子号</th>
					<th  class="sort-column brand">品牌</th>
				<th  class="sort-column types">种类</th>
				<th  class="sort-column types">商品数量</th>
				<th  class="sort-column updateDate">更新时间</th>
				<th  class="sort-column  createBy.id">操作人</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="markBox">
			<tr>
				<td> <input type="checkbox" id="${markBox.id}" class="i-checks"></td>
				<td><a  href="#" onclick="openDialogView('查看保存箱子', '${ctx}/markbox/markBox/form?id=${markBox.id}','800px', '500px')">
					${markBox.mcode}
				</a></td>
				<td>
					${fns:getDictLabel(markBox.brand, 'item_brand', '')}
				</td>
				<td>
					${fns:getDictLabel(markBox.types, 'item_kind', '')}
				</td>
				<td>
					${markBox.itemcount}
				</td>
				<td>
					<fmt:formatDate value="${markBox.updateDate}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${markBox.createBy.name}
				</td>
				<td>
					<shiro:hasPermission name="markbox:markBox:edit">
    					<a href="#" onclick="openDialog('修改箱子', '${ctx}/markbox/markBox/form?id=${markBox.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
    				</shiro:hasPermission>
					<shiro:hasPermission name="markitem:markItem:list">
						<a href="${ctx}/markitem/markItem/xzsp?markbox.id=${markBox.id}" class="btn btn-info btn-xs J_menuItem" ><i class="fa fa-search-plus"></i> 查看详情</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>