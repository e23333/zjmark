<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>罚没商品信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
					laydate({
			            elem: '#firstinputtime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
			            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			        });
		});
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="markItem" action="${ctx}/markitem/markItem/save3" method="post" class="form-horizontal" enctype="multipart/form-data">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				
				<tr>
					<td class="width-15 active"><label class="pull-right">英文品牌：</label></td>
					<td class="width-35">
                         <form:select path="brand" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('item_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						   </form:select>					
						   </td>
					<td class="width-15 active"><label class="pull-right">英文品牌是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="brandshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">中文品牌：</label></td>
					<td class="width-35">
                          <form:select path="zhongbrand" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('item_brand')}" itemLabel="description" itemValue="value" htmlEscape="false"/>
						   </form:select>					
					</td>
					<td class="width-15 active"><label class="pull-right">中文品牌是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="zhongbrandshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">种类：</label></td>
					<td class="width-35">
						<form:select path="types" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('item_kind')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right">种类是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="typesshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">原产地：</label></td>
					<td class="width-35">
						<form:input path="origin" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">原产地是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="originshow" items="${fns:getDictList('isshow')}"   itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
				
					<td class="width-15 active"><label class="pull-right">正面照片：</label></td>
					<td class="width-35">
					<c:if test="${markItem.zhengimg != null && markItem.zhengimg != ''}">
					   <img src="${pageContext.request.contextPath}/${markItem.zhengimg}" width="100" height="100"/>
					</c:if>
					<form:input path="zheng"  type="file" />
					</td>
					<td class="width-15 active"><label class="pull-right">正面照片是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="zhengimgshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">反面照片：</label></td>
					<td class="width-35">
						<c:if test="${markItem.fanimg != null && markItem.fanimg != ''}">
					   <img src="${pageContext.request.contextPath}/${markItem.fanimg}" width="100" height="100"/>
					</c:if>
					<form:input path="fan"  type="file" />
					</td>
					<td class="width-15 active"><label class="pull-right">反面照片是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="fanimgshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">贴签后照片：</label></td>
					<td class="width-35">
						<c:if test="${markItem.biaoqianimg != null && markItem.biaoqianimg != ''}">
					   <img src="${pageContext.request.contextPath}/${markItem.biaoqianimg}" width="100" height="100"/>
					</c:if>
					<form:input path="biaoqian"  type="file" />
					</td>
					<td class="width-15 active"><label class="pull-right">贴签后照片是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="biaoqianimgshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">其他照片：</label></td>
					<td class="width-35">
						<c:if test="${markItem.otherimg != null && markItem.otherimg != ''}">
					   <img src="${pageContext.request.contextPath}/${markItem.otherimg}" width="100" height="100"/>
					</c:if>
					<form:input path="other"  type="file" />
					</td>
					<td class="width-15 active"><label class="pull-right">其他照片是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="otherimgshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">标签值：</label></td>
					<td class="width-35">
						<form:input path="mcode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">二维码内容：</label></td>
					<td class="width-35">
						<form:input path="qrcode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">需补录标志：</label></td>
					<td class="width-35">
						<form:radiobuttons path="needsupply" items="${fns:getDictList('s_yesOrNo')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
					<td class="width-15 active"><label class="pull-right">最初绑定时间：</label></td>
					<td class="width-35">
						<input id="firstinputtime" name="firstinputtime" type="text" maxlength="20" class="laydate-icon form-control layer-date "
							value="<fmt:formatDate value="${markItem.firstinputtime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"></td>
					 <td class="width-35">	
					</td>
					<td class="width-15 active"><label class="pull-right">材质是否显示：</label></td>
					<td class="width-35">
						<form:radiobuttons path="materialshow" items="${fns:getDictList('isshow')}"  itemLabel="label" itemValue="value" htmlEscape="false" class="i-checks "/>
					</td>
				</tr>
		 	</tbody>
		</table>
		 <div class="tabs-container">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">材质：</a>
                </li>
            </ul>
            <div class="tab-content">
				<div id="tab-1" class="tab-pane active">
			<a class="btn btn-white btn-sm" onclick="addRow('#ChildList', testDataChildRowIdx, testDataChildTpl);testDataChildRowIdx = testDataChildRowIdx + 1;" title="新增"><i class="fa fa-plus"></i> 新增</a>
            <table id="contentTable" class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th class="hide"></th>
						<th>材质名</th>
						<th>百分比</th>
						<th>使用位置</th>
						<th width="10">&nbsp;</th>
					</tr>
				</thead>
				<tbody id="ChildList"></tbody>
			</table>
				<script type="text/template" id="testDataChildTpl">//<!--
				<tr id="ChildList{{idx}}">
					<td class="hide">
						<input id="ChildList{{idx}}_id" name="ChildList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
						<input id="ChildList{{idx}}_delFlag" name="ChildList[{{idx}}].delFlag" type="hidden" value="0"/>
					</td>

					<td class="width-35">
						<select id="ChildList{{idx}}_name" name="ChildList[{{idx}}].name" class="form-control ">					
                            <c:forEach items="${fns:getDictList('item_materialname')}" var="tt">
                             <option   value="${tt.value}" >${tt.label}</option>
                            </c:forEach>
						<select>
					</td>
				
					
					
					<td>
						<input id="ChildList{{idx}}_percentage" name="ChildList[{{idx}}].percentage" type="text" value="{{row.percentage}}"  onkeyup="this.value=this.value.replace(/[^\d]/g,'') "   class="form-control required"/>
					</td>
					
					
					 <td class="width-35">
						<select id="ChildList{{idx}}_remark" name="ChildList[{{idx}}].remark" class="form-control ">					
                            <c:forEach items="${fns:getDictList('material_position')}" var="tt">
                             <option   value="${tt.value}" >${tt.label}</option>
                            </c:forEach>
						<select>
					</td>
					
					<td class="text-center" width="10">
						{{#delBtn}}<span class="close" onclick="delRow(this, '#ChildList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
					</td>
				</tr>//-->
			</script>
			<script type="text/javascript">
				var testDataChildRowIdx = 0, testDataChildTpl = $("#testDataChildTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
				$(document).ready(function() {
					var data = ${fns:toJson(markItem.childList)};
					for (var i=0; i<data.length; i++){
						addRow('#ChildList', testDataChildRowIdx, testDataChildTpl, data[i]);
						testDataChildRowIdx = testDataChildRowIdx + 1;
						var pxx=data[i].name;
						 $("#ChildList"+i+"_name").find("option[value='"+pxx+"']").attr("selected",true);
						 var pxx1=data[i].remark;
						 $("#ChildList"+i+"_remark").find("option[value='"+pxx1+"']").attr("selected",true);
					}
				});
			</script>
			</div>		
		  </div>	
	
		
	</form:form>
</body>
</html>