<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>补录商品信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>补录商品信息列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="markItem" action="${ctx}/markitem/markItem/bl" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>原产地：</span>
				 <form:select path="origin"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('origin')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				  </form:select>
			<span>品牌：</span>
                   <form:select path="brand"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('item_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				  </form:select>			
				<span>种类：</span>
				<form:select path="types"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('item_kind')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span>二维码：</span>
	        	<form:input path="qrcode"/>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column itembox.mcode">箱子号</th>
				<th  class="sort-column brand">     英文品牌</th>
				<th  class="sort-column zhongbrand">中文品牌</th>
				<th  class="sort-column types">        种类</th>
				<th  class="sort-column origin">      原产地</th>
				<th  class="sort-column qrcode">    二维码</th>
				<th  class="sort-column needsupply">需补录标志</th>
				<th  class="sort-column firstinputtime">最初绑定时间</th>
				<th  class="sort-column createBy.id">操作人</th>
				<th  class="sort-column cancelmark">状态</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="markItem">
			<tr>
				<td>
				${markItem.markbox.mcode}
				</td>
				
				<td>
						${fns:getDictLabel(markItem.brand, 'item_brand', '')}
				</td>
				<td>
					${fns:getDictDescription(markItem.zhongbrand, 'item_brand', '')}
				</td>
				<td>
					${fns:getDictLabel(markItem.types, 'item_kind', '')}
				</td>
				<td>
				${fns:getDictLabel(markItem.origin, 'origin', '')}
				</td>
				<td>	       
				      ${markItem.qrcode}
				</td>
				<td>
					${fns:getDictLabel(markItem.needsupply, 's_yesOrNo', '')}
				</td>
				<td>
					<fmt:formatDate value="${markItem.firstinputtime}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
				${markItem.createBy.name}
				</td>
				<td>
					${fns:getDictLabel(markItem.cancelmark, 'cancelmark', '')}
				</td>
				<td>
					<shiro:hasPermission name="markitem:markItem:edit">
    					<a href="#" onclick="openDialog('补录罚没商品信息', '${ctx}/markitem/markItem/form3?id=${markItem.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 补录</a>
    				</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>