<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>罚没商品信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>罚没商品信息列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="markItem" action="${ctx}/markitem/markItem/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
		<span>箱子号：</span>
		<form:input path="markbox.mcode" id="aa"/>
			<span>原产地：</span>
				 <form:select path="origin"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('origin')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				  </form:select>
			<span>品牌：</span>
                   <form:select path="brand"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('item_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				  </form:select>			
				<span>种类：</span>
				<form:select path="types"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('item_kind')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span>二维码：</span>
	        	<form:input path="qrcode"/>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="markitem:markItem:add">
				<table:addRow url="${ctx}/markitem/markItem/form" title="罚没商品信息"></table:addRow><!-- 增加按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="markitem:markItem:edit">
			    <table:editRow url="${ctx}/markitem/markItem/form" title="罚没商品信息" id="contentTable"></table:editRow><!-- 编辑按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="markitem:markItem:del">
				<table:delRow url="${ctx}/markitem/markItem/deleteAll" id="contentTable"></table:delRow><!-- 删除按钮 -->
			</shiro:hasPermission>
			 <%--<table:exportExcel url="${ctx}/markitem/markItem/export"></table:exportExcel><!-- 导出按钮 -->--%>
			<button id="btnExport" class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" title="导出"><i class="fa fa-file-excel-o"></i> 导出</button>
			<script type="text/javascript">
                $(document).ready(function() {

                    $("#btnExport").click(function(){
                        top.layer.confirm('确认要导出Excel吗?', {icon: 3, title:'系统提示'}, function(index){
                            //do something
                            //导出之前备份
                            var url =  $("#searchForm").attr("action");
                            var pageNo =  $("#pageNo").val();
                            var pageSize = $("#pageSize").val();
                            //导出excel
							var origin1=$('#origin').val();
							var brand1=$('#brand').val();
							var types1=$('#types').val();
							var qrcode1=$('#qrcode').val();
							//var mcode2=$("#markbox\\.mcode").val();
                            var mcode1= $(":input[name='markbox.mcode']").val();
                            $("#searchForm").attr("action","${ctx}/markitem/markItem/export?origin="+origin1+"&brand="+brand1+"&types="+types1+"&qrcode="+qrcode1+"&mcode="+mcode1+"");
                            $("#pageNo").val(-1);
                            $("#pageSize").val(-1);
                            $("#searchForm").submit();

                            //导出excel之后还原
                            $("#searchForm").attr("action",url);
                            $("#pageNo").val(pageNo);
                            $("#pageSize").val(pageSize);
                            top.layer.close(index);
                        });
                    });

                });

			</script><!-- 导出按钮 -->
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th> <input type="checkbox" class="i-checks"></th>
				<th  class="sort-column itembox.mcode">箱子号</th>
				<th  class="sort-column brand">     英文品牌</th>
				<th  class="sort-column zhongbrand">中文品牌</th>
				<th  class="sort-column types">     种类</th>
				<th  class="sort-column origin">    原产地</th>
				<th  class="sort-column qecode">    二维码</th>
				<th  class="sort-column needsupply">需补录标志</th>
				<th  class="sort-column firstinputtime">最初绑定时间</th>
				<th  class="sort-column createBy.id">操作人</th>
				<th  class="sort-column cancelmark">状态</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="markItem">
			<tr>
				<td> <input type="checkbox" id="${markItem.id}" class="i-checks"></td>
				<td>
				${markItem.markbox.mcode}
				</td>
				<td>
					${fns:getDictLabel(markItem.brand, 'item_brand', '')}
				</td>
				<td>
					${fns:getDictDescription(markItem.zhongbrand, 'item_brand', '')}
				</td>
				<td>
					${fns:getDictLabel(markItem.types, 'item_kind', '')}
				</td>
				<td>
		       
				      ${fns:getDictLabel(markItem.origin, 'origin', '')}
				</td>
				<td>
		       
				      ${markItem.qrcode}
				</td>
				<td>
					${fns:getDictLabel(markItem.needsupply, 's_yesOrNo', '')}
				</td>
				<td>
					<fmt:formatDate value="${markItem.firstinputtime}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
				${markItem.createBy.name}
				</td>
				<td>
					${fns:getDictLabel(markItem.cancelmark, 'cancelmark', '')}
				</td>
				<td>
					<shiro:hasPermission name="markitem:markItem:edit">
    					<a href="#" onclick="openDialog('修改罚没商品信息', '${ctx}/markitem/markItem/form?id=${markItem.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
    				</shiro:hasPermission>
    				<c:if test="${markItem.cancelmark==1}">
    				<shiro:hasPermission name="markitem:markItem:edit"> 				
    					<a href="${ctx}/markitem/markItem/zx2?id=${markItem.id}"   class="btn btn-warning btn-xs " ><i class="fa fa-edit"></i> 注销</a>
    				</shiro:hasPermission>
    				</c:if>
    				<shiro:hasPermission name="markitem:markItem:del">
						<a href="${ctx}/markitem/markItem/delete?id=${markItem.id}" onclick="return confirmx('确认要删除该罚没商品信息吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>