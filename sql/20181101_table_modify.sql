alter table mark_item add column requestcode varchar(64) COMMENT '申请号';
alter table mark_item add column securitycode varchar(128) COMMENT '品牌防伪标';
alter table mark_item add column modelinfo varchar(128) COMMENT '型号';
alter table mark_item add column material varchar(128) COMMENT '材质';
alter table mark_item add column diameter varchar(64) COMMENT '表径';
alter table mark_item add column maintenanceflag varchar(2) COMMENT '是否保养：0否1是';
alter table mark_item add column fineness varchar(128) COMMENT '成色';
alter table mark_item add column price decimal(10,2) COMMENT '价格';
alter table mark_item add column finalprice decimal(10,2) COMMENT '公价';
alter table mark_item add column styleinfo varchar(128) COMMENT '款式';
alter table mark_item add column otherinfo varchar(256) COMMENT '其它';
alter table mark_item add column auditstatus varchar(2) COMMENT '审核状态：0未提交1待审核2审核通过3审核驳回';



-- ----------------------------
--  Table structure for `mark_item_file`
-- ----------------------------
DROP TABLE IF EXISTS `mark_item_file`;
CREATE TABLE `mark_item_file` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'id',
  `item_id` varchar(64)  COMMENT '产品id',
  `url` varchar(255)  COMMENT 'url',
  `showname` char(64)  COMMENT '图片描述',
  `filename` varchar(64) COMMENT '文件名',
  `seq` tinyint DEFAULT 99 COMMENT '序号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` date DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` date DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`),
  CONSTRAINT  fk_mark_item1 FOREIGN KEY (item_id) REFERENCES mark_item(id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='mark_item_file';