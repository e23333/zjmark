-- 催报历史表
create table s_report_history (
  id  varchar(64) not null PRIMARY KEY ,
  sid varchar(64) not null COMMENT '企业表主键',
  businessType int not null COMMENT '业务类型，0: 生活必需品',
  sendType int not null COMMENT '催报类型 0: 电话，1：短信，2：微信',
  sendTo varchar(100) not null COMMENT '发送给谁',
  sendContent varchar(1000) COMMENT '催报内容',
  sendTime date not null COMMENT '催报时间',
  sendUserId varchar(64) not null COMMENT '发送人id'
);

-- 商品分类表
create table s_report_model (
  id varchar(64) not null PRIMARY KEY,

  name varchar(100) COMMENT '模板名称',
  current_period char(1) DEFAULT '0' COMMENT '当期 0: 显示， 1：隐藏',
  same_period char(1) DEFAULT '0' COMMENT '同期 0: 显示， 1：隐藏',
  per_cent char(1) DEFAULT '0' COMMENT '同比 0: 显示， 1：隐藏',
  the_chain char(1) DEFAULT '0' COMMENT '同比 0: 显示， 1：隐藏',
  class LONGTEXT COMMENT '商品分类 存储分类id',

  create_by varchar(64),
  create_date DATETIME,
  update_by varchar(64),
  update_date DATETIME,
  del_flag char(1) DEFAULT '0'
);
alter table s_report_model COMMENT '统计模板';