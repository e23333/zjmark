INSERT INTO sys_dict ( id, value, label, type, description, sort, parent_id, create_by, create_date, update_by, update_date, remarks, del_flag ) VALUES ('10a69a72fc5445e697bb99a20deaf268', '0', '有效', 's_contact_flag', '联系人是否有效', '10', '0', 'admin', '2017-06-14 11:40:28', 'admin', '2017-06-14 11:40:28', '', '0');
INSERT INTO sys_dict ( id, value, label, type, description, sort, parent_id, create_by, create_date, update_by, update_date, remarks, del_flag ) VALUES ('183ecd1544544de3a305d344d3e4e5a5', '1', '注销', 's_contact_flag', '联系人是否有效', '20', '0', 'admin', '2017-06-14 11:40:40', 'admin', '2017-06-14 11:40:40', '', '0');

INSERT INTO sys_dict ( id, value, label, type, description, sort, parent_id, create_by, create_date, update_by, update_date, remarks, del_flag ) VALUES ('10a69a72fc5445e697bb99a20deaf269', '0', '启用', 's_enterprise_flag', '企业是否停用', '10', '0', 'admin', '2017-06-14 11:40:28', 'admin', '2017-06-14 11:40:28', '', '0');
INSERT INTO sys_dict ( id, value, label, type, description, sort, parent_id, create_by, create_date, update_by, update_date, remarks, del_flag ) VALUES ('183ecd1544544de3a305d344d3e4e5a6', '1', '停用', 's_enterprise_flag', '企业是否停用', '20', '0', 'admin', '2017-06-14 11:40:40', 'admin', '2017-06-14 11:40:40', '', '0');


DROP TABLE IF EXISTS s_enterprise_log;
CREATE TABLE s_enterprise_log (
  sid int(10) NOT NULL COMMENT 'LOGID',
  enterprise_id int(10) NOT NULL COMMENT '企业ID',
  enterprise_name varchar(100) DEFAULT NULL COMMENT '企业名称',
  optype tinyint(1) DEFAULT 0 COMMENT '状态：0-启用 1-停用',
  optime date COMMENT '操作时间',
  oper varchar(36) COMMENT '操作人',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业启用停用日志';